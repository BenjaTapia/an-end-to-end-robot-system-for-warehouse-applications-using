**Run full implementation in coppeliaSim simulator**

## CoppeliaSim program (CoppeliaSim EDU version used 4.2.0)

the archive warehousemission.ttt is the coppeliaSim program were LUA implementation is made 

## Python archives  (python version used 3.7.7)

make sure that the archives: 
1)interpreter.py
2)libFunciones.py
3)remoteApi.dll
4)sim.py
5)simConst.py
6)tresestantesLTL.txt 
7)problema-tresestantes.py
are in the same path

## How to run CoppeliaSim program 
use these steps to run the warehouse mission in CoppeliaSim 

1)open warehousemission.ttt
2)open with IDLE problema-tresestantes.py
3)run problema-tresestantes.py
and then thesimulation in CoppeliaSim environment will start 

## How to see LTS intructions 

1) Download and install MTSA 
you can see the install guide there https://mtsa.dc.uba.ar/
2) open tresestantes.lts with MTSA
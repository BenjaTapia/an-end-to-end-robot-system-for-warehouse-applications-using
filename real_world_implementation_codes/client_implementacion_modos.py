import socket
import time
import numpy as np
import serial,time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = '172.20.10.6';  #Cambiar por la ip de la notebook

s.connect((ip, 50000))

message = b''
if __name__ == '__main__':
    
    print('Running. Press CTRL-C to exit.')
    with serial.Serial("/dev/ttyACM0", 115200, timeout=1) as arduino:
        time.sleep(0.1) #wait for serial to open
        if arduino.isOpen():
            print("{} connected!".format(arduino.port))
            while 1:
                amount_received = 0
                amount_expected = 12 #bytes (modificar por el length de lo que tira el servidor
                message = b''
                while amount_received < amount_expected:
                    data = s.recv(1)
                    message += data
                    amount_received += len(data)

                array = np.frombuffer(message,np.int)
                angulo_actual=int(array[0])
                angulo_obj=int(array[1])
                distancia=int(array[2])
                modo= int(array[3])
                print("message: ",array)
                arduino.write(bytearray([int(angulo_actual/255),angulo_actual%255,int(angulo_obj/255),angulo_obj%255,int(distancia/255),distancia%255,int(modo/255),modo%255]))
                #aca recibo para debugear arduino
                
                #devolucion=arduino.readline()
                #print("devolucion arduino",devolucion)
                
                #...
            arduino.close()   
            s.close()



//// MOTOR A Izquierdo
int Mizq = 10;
int IN1 = 9;
int IN2 = 8;

//// MOTOR B Derecho
int Mder = 5;
int IN3 = 7;
int IN4 = 6;

//offset de los motores
int offset = 120;

void Giro_Derecha(int V_der)
{
    if (V_der>0)
    {
        digitalWrite (IN1, HIGH);
        digitalWrite (IN2, LOW);
        analogWrite (Mder, V_der);
    }

    else
    {
        V_der=abs(V_der);
        digitalWrite (IN1, LOW);
        digitalWrite (IN2, HIGH);
        analogWrite (Mder,V_der);
    }

}

void Giro_Izquierda(int V_izq)
{
    if (V_izq>0)
    {
        digitalWrite (IN1, HIGH);
        digitalWrite (IN2, LOW);
        analogWrite (Mizq, V_izq);
    }

    else
    {
        V_izq=abs(V_izq);
        digitalWrite (IN1, LOW);
        digitalWrite (IN2, HIGH);
        analogWrite (Mizq,V_izq);
    }

}

int control_angulo(int objetivo, int actual)   //lleVa el angulo del robot al angulo objetiVo
{
    int Velocidad=0;
    double dif = 0;
    double V = 0;
    dif = objetivo - actual;
    //Serial.print(dif);
    if (dif > 180)
    {
        dif = dif - 360;
    }

    if (dif < -180)
    {
        dif = dif + 360;
    }
    //Serial.println("dif");
    //Serial.print(dif);
    double K_tuneado = 0.5; //para tunear
    V = dif * K_tuneado;
    //Serial.print(V);


    if (abs(dif) < 5)
    {
        V=0;

    }
    else
    {
        if (V>0)
        {
            //Serial.println("V mayor 0");
            V=V+offset;
            if (V>249)
            {
                V=250;
            }

        }
        else
        {
            //Serial.println("V menor 0");
            V=V-offset;
            if (V<-249)
            {
                V=-250;
            }
        }

    }
    Velocidad= int(V);
    return Velocidad;
}

int control_posicion(int distancia)
{
    int velocidad=0;
    if (distancia>30)
    {
        velocidad=10+offset;
    }
    else
    {

        velocidad=0;
    }
    return velocidad;
}



int control_total(int actual, int objetivo, int distancia)
{

    int dif_angulo=0;
    int vdif=0;
    int vprom=0;
    int v_der=0;
    int v_izq=0;

    if (distancia>50)
    {
        if (dif_angulo>30)
        {
            vdif=control_angulo(objetivo,actual);
            if (vdif<0)
            {
                v_der=vdif;
                v_izq=-1*vdif;
                Giro_Derecha(v_der);
                Giro_Izquierda(v_izq);

            }
            else
            {
                v_izq=vdif;
                v_der=-1*vdif;
                Giro_Derecha(v_der);
                Giro_Izquierda(v_izq);

            }
        }

    }
    else
    {

        vdif=control_angulo(objetivo,actual);
        vprom=control_posicion(distancia);
        if (vdif<0)
        {
            v_der=vdif+vprom;
            v_izq=-1*vdif+vprom;
            Giro_Derecha(v_der);
            Giro_Izquierda(v_izq);

        }
        else
        {
            v_izq=vdif+vprom;
            v_der=-1*vdif+vprom;
            Giro_Derecha(v_der);
            Giro_Izquierda(v_izq);

        }

    }




}
void setup()
{
    Serial.begin(115200);
    pinMode (Mizq, OUTPUT);
    pinMode (Mder, OUTPUT);
    pinMode (IN1, OUTPUT);
    pinMode (IN2, OUTPUT);
    pinMode (IN3, OUTPUT);
    pinMode (IN4, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

void loop()
{
    int angulo_actual=0;
    int angulo_obj=0;
    int distancia=0;
    int contador = 0;
    int VelocidadMotores=0;
    int timeout = 0;
    int dt = 5;


    while (1) //esta es la parte donde leo los datos de la raspi
    {
        if (Serial.available() > 0)
        {
            int aux = (int)Serial.read() ;
            contador = contador + 1;
            if (contador == 1)
            {
                angulo_actual = 255 * aux;

            }
            if (contador == 2)
            {
                angulo_actual = angulo_actual + aux;

            }
            if (contador == 3)
            {
                angulo_obj = 255*aux;

            }
            if (contador == 4)
            {
                angulo_obj=angulo_obj+aux;
            }
            if (contador == 5)
            {
                distancia = 255*aux;

            }
            else if (contador == 6)
            {
                distancia=distancia+aux;
                contador=0;
                break;
            }
        }
        else
        {
            timeout = timeout + 1;
            if (timeout > 2000/dt)
            {
                //reset motores
                Giro_Derecha(0);
                Giro_Izquierda(0);
                timeout = 0;
            }
            delay(dt);
        }
    }
}

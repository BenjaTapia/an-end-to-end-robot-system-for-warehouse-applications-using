from interpreter import Automata
import time
from multiprocessing import Value

x_comp = Value('i',0)
y_comp = Value('i',0)
modo_comp = Value('i',1)

#Definir funciones controlables del plan

def go(x,y):
    global x_comp
    global y_comp
    global modo_comp
    
    x = int(x)
    y = int(y)
    
    print ('going to {:d} {:d}'.format(x,y))

    #Necesito meter esto en el coppelia
    x_comp.value = x
    y_comp.value = y
    modo_comp.value=1
   
def goR(x,y):
    global x_comp
    global y_comp
    global modo_comp
    
    x = int(x)
    y = int(y)
    
    print ('goingR to {:d} {:d}'.format(x,y))

    #Necesito meter esto en el coppelia
    x_comp.value = x
    y_comp.value = y
    modo_comp.value=3

def agarrar_caja(k,x,y):
    global x_comp
    global y_comp
    global modo_comp
    
    x = int(x)
    y = int(y)
    print ('agarrando caja en {:d} {:d}'.format(x,y))
    modo_comp.value=4
    time.sleep(0.25)
def soltar_caja(k,x,y):
    global x_comp
    global y_comp
    global modo_comp
    
    x = int(x)
    y = int(y)
    print ('soltando caja en {:d} {:d}'.format(x,y))
    modo_comp.value=5
    time.sleep(0.25)
#Creo lista de funciones con acciones controlables
function_list = []
function_list.append(go)
function_list.append(goR)
function_list.append(soltar_caja)
function_list.append(agarrar_caja)


automata = Automata(function_list,verbose=True)
event_queue = automata.get_event_queue() #Necesitamos la cola de eventos
automata.load_automata_from_file('tresestantesLTL.txt') #Cargamos el plan

#inicio comunicacion con coppelia
try:
    import sim
except:
    print ('--------------------------------------------------------------')
    print ('"sim.py" could not be imported. This means very probably that')
    print ('either "sim.py" or the remoteApi library could not be found.')
    print ('Make sure both are in the same folder as this file,')
    print ('or appropriately adjust the file "sim.py"')
    print ('--------------------------------------------------------------')
    print ('')

import time
import libFunciones as lf

print ('Program started')
sim.simxFinish(-1) # just in case, close all opened connections
clientID=sim.simxStart('127.0.0.1',19997,True,True,5000,5) # Connect to CoppeliaSim

if clientID!=-1:
    print ('Connected to remote API server')

    #Arrancar la simulacion o pararla si ya esta iniciada
    lf.restart(clientID)
    print("Simulation started")

    #Ejemplo de como se le puede setear una variable del coppelia
    #para enviar informacion, por ejemplo que tipo de accion se tiene
    #que correr
   
    time.sleep(2)
    #Arrancamos el plan
    automata.start() # <-- proceso independiente

    #A partir de aca podes correr cualquier cosa
    time.sleep(1)
   
    opnumber=sim.simx_opmode_streaming
    devolucion_modo,llegue=sim.simxGetIntegerSignal(clientID,'llegue',opnumber)
    opnumber=sim.simx_opmode_buffer
    llegue_ant=0
    while (1): # <-- comunicacion con el arduino
        #leyendo del copelia los valores de posicion
        
        x = x_comp.value
        y = y_comp.value
        modo = modo_comp.value
        
        # print('modo')
        #print(modo)
        if modo==1:
            
            print('yendo en el coppelia')
            lf.tarea(clientID,x,y,modo)
            modo_comp.value=0
            modo = modo_comp.value

        if modo==3:
            
            
            print('yendo en reversa en el coppelia')
            lf.tarea(clientID,x,y,modo)
            modo_comp.value=0
            modo = modo_comp.value

        if modo==4:
            print('agarrando en el coppelia')
            lf.controlServo(clientID,modo)
            
            modo_comp.value=0
            modo = modo_comp.value
            
            
        if modo==5:
            print('soltando en el coppelia')
            lf.controlServo(clientID,modo)
            
            modo_comp.value=0
            modo = modo_comp.value
            


            
       #print('---------------------------')
        devolucion_modo,llegue=sim.simxGetIntegerSignal(clientID,'llegue',opnumber)
        #print(devolucion_modo)
        #print('llegue',llegue)
        if llegue==1 and llegue_ant==0:
            event_queue.put('arrived[{:d}][{:d}]'.format(x,y))
            
            print('llegue')
            print (llegue)
            #lf.setVariable(clientID,'llegue',0)
        llegue_ant=llegue    
        time.sleep(0.05)
else:
    print ('Failed connecting to remote API server')
print ('Program ended')

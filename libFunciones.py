import sim

def restart(clientID):
    res = sim.simxStopSimulation(clientID,sim.simx_opmode_blocking);
    is_running = True;
    while is_running:
        error_code, ping_time = sim.simxGetPingTime(clientID);
        error_code, server_state = sim.simxGetInMessageInfo(clientID, sim.simx_headeroffset_server_state);
        
        is_running = server_state & 1;
    sim.simxStartSimulation(clientID,sim.simx_opmode_blocking);

def setVariable(clientID,var,value):
	res = sim.simxSetIntegerSignal(clientID,var,value,sim.simx_opmode_oneshot)


def tarea(clientID,x,y,modo):
    setVariable(clientID,"x_obj",x)
    setVariable(clientID,"y_obj",y)
    setVariable(clientID,"modo",modo)

def controlServo(clientID,modo):
    setVariable(clientID,"modo",modo)
  
    
